using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Progress : MonoBehaviour
{
    #region Singleton
    public static Progress Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);

            return;
        }

        Destroy(gameObject);
    }
    #endregion

    public int CharacterID { get; private set; } = 1;
    public int PeriodID { get; private set; } = 1;
    public int PartID { get; private set; } = 1;
    public string Responses { get; private set; } = "/1.3:1";



    public void AddResponse(ResponseConfig responseConfig)
    {
        Responses += $"/{responseConfig.CaseID}.{responseConfig.ResponseID}:{responseConfig.Response}";
    }
}
