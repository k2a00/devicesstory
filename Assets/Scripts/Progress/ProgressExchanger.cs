using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProgressExchanger : MonoBehaviour
{
    [HideInInspector] public UnityEvent OnProgressLoaded;


    public void Save()
    {

    }

    public void Load()
    {
        OnProgressLoaded?.Invoke();
    }
}
