public enum Keys
{
    Check,
    App,
    IDDialog,
    DialogName,
    From,
    Subject,
    Text,
    Delay,
    Variants
}