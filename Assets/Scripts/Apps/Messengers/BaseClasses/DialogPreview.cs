using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public abstract class DialogPreview : MonoBehaviour
{
    [SerializeField] protected Text tittle;
    [SerializeField] protected Text subtittle;

    [HideInInspector] public UnityEvent OnPreviewClicked;
    

    public virtual void Init(CaseConfig message)
    {
        tittle.text = message.Data[Keys.From];
        subtittle.text = message.Data[Keys.Text];
    }

    public virtual void ChangeSubtittle(string _subtittle)
    {
        subtittle.text = _subtittle;
    }

    public virtual void OpenDialog()
    {
        OnPreviewClicked?.Invoke();
    }
}
