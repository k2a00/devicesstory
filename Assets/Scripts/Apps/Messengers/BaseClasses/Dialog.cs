using UnityEngine;
using System.Collections;

public abstract class Dialog : MonoBehaviour, IResponseListener
{
    [SerializeField] protected GameObject view;
    [SerializeField] protected InterlocutorMessageView interlocutorMessageViewPrefab;
    [SerializeField] protected PlayerMessageView playerResponseViewPrefab;
    [SerializeField] protected Transform parentForMessages;
    [SerializeField] protected ResponseButton responseButton;
    [SerializeField] protected VariantsOfResponse variantsOfResponse;



    public virtual void Init(CaseConfig caseConfig, DialogPreview preview, IResponseListener responseListener)
    {
        AddInterlocutorMessage(caseConfig);

        preview.OnPreviewClicked.AddListener(OpenDialog);

        responseButton.AddListener(responseListener);

        responseButton.AddListener(this);
    }

    public virtual void OpenDialog()
    {
        view.SetActive(true);
    }

    public virtual void CloseDialog()
    {
        view.SetActive(false);
    }

    public virtual void AddInterlocutorMessage(CaseConfig caseConfig)
    {
        StartCoroutine(SendInterlocutorMessage(caseConfig));
    }

    public void SetResponse<T>(T responseSender) where T : IResponseSendable
    {
        AddPlayerMessage(responseSender.GetResponseVariantInfo().Text);
    }

    protected void AddPlayerMessage(string playerResponseText)
    {
        PlayerMessageView playerMessageView = Instantiate(playerResponseViewPrefab, parentForMessages);

        playerMessageView.Init(playerResponseText);
    }

    protected IEnumerator SendInterlocutorMessage(CaseConfig caseConfig)
    {
        int delay = 1;

        if (caseConfig.Data[Keys.Delay] != "")
        {
            delay = int.Parse(caseConfig.Data[Keys.Delay]);
        }

        yield return new WaitForSeconds(delay);

        InterlocutorMessageView interlocutor = Instantiate(interlocutorMessageViewPrefab, parentForMessages);

        interlocutor.Init(caseConfig);

        variantsOfResponse.InstantiateResponseVariants(caseConfig);
    }
}
