using System.Collections.Generic;
using UnityEngine;

public abstract class Messenger : App, ICaseConfigHandler
{
    [SerializeField] protected Transform dialogPreviewParent;
    [SerializeField] protected DialogPreview dialogPreviewPrefab;
    [SerializeField] protected Transform dialogParent;
    [SerializeField] protected Dialog dialogPrefab;

    protected Dictionary<string, Dialog> dialogsStorage = new Dictionary<string, Dialog>();


    public abstract List<Keys> GetDataConfig();

    public virtual void InitializeProcessing(CaseConfig caseConfig, IResponseListener responseListener)
    {
        Dialog dialog = Instantiate(dialogPrefab, dialogParent);

        DialogPreview dialogPreview = Instantiate(dialogPreviewPrefab, dialogPreviewParent);

        dialog.Init(caseConfig, dialogPreview, responseListener);

        dialogPreview.Init(caseConfig);

        dialogsStorage.Add(caseConfig.Data[Keys.IDDialog], dialog);
    }

    public virtual bool TryToProcessCaseConfig(CaseConfig caseConfig)
    {
        Dialog dialog;

        if (dialogsStorage.TryGetValue(caseConfig.Data[Keys.IDDialog], out dialog))
        {
            dialog.AddInterlocutorMessage(caseConfig);

            return true;
        }

        return false;
    }
}