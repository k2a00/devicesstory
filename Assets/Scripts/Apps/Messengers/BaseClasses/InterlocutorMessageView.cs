using UnityEngine;
using UnityEngine.UI;

public abstract class InterlocutorMessageView : MonoBehaviour
{
    [SerializeField] protected Text mainText;

    public virtual void Init(CaseConfig message)
    {
        mainText.text = message.Data[Keys.Text];
    }
}
