using UnityEngine;
using UnityEngine.UI;

public class PlayerMessageView : MonoBehaviour
{
    [SerializeField] private Text mainText;

    public void Init(string responseText)
    {
        mainText.text = responseText;
    }
}
