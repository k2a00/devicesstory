using System.Collections.Generic;

public sealed class ShortMessageService : Messenger
{
    protected override List<Keys> DataConfig { get; set; } = new List<Keys>
            {
                Keys.IDDialog,
                Keys.Delay,
                Keys.From,
                Keys.Text,
                Keys.Variants
            };

    public override List<Keys> GetDataConfig()
    {
        return DataConfig;
    }
}
