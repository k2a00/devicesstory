using UnityEngine.UI;
using UnityEngine;

public sealed class SMSDialog : Dialog
{
    [SerializeField] private Text senderTittle;


    public override void Init(CaseConfig message, DialogPreview preview, IResponseListener responseListener)
    {
        base.Init(message, preview, responseListener);

        senderTittle.text = message.Data[Keys.From];
    }
}
