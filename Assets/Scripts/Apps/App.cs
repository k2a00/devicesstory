using System.Collections.Generic;
using UnityEngine;

public abstract class App : MonoBehaviour
{
    [SerializeField] protected AppKeyWords appKeyWord;
    [SerializeField] protected GameObject view;

    protected abstract List<Keys> DataConfig { get; set; }


    public AppKeyWords GetAppKeyWord() => appKeyWord;

    public virtual void ShowApp()
    {
        view.SetActive(true);
    }

    public virtual void HideApp()
    {
        view.SetActive(false);
    }
}
