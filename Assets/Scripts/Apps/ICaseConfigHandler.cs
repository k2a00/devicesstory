using System.Collections.Generic;

public interface ICaseConfigHandler
{
    public List<Keys> GetDataConfig();
    public void InitializeProcessing(CaseConfig message, IResponseListener responseListener);
    public bool TryToProcessCaseConfig(CaseConfig message);
}