using System.Collections.Generic;
using UnityEngine;

public class AppsStorage : MonoBehaviour
{
    [SerializeField] private List<App> appsPrefabs;
    [SerializeField] private Transform parent;

    private List<App> loadedApps = new List<App>();


    public App GetApp(string keyWord)
    {
        for (int i = 0; i < loadedApps.Count; i++)
        {
            if (keyWord == loadedApps[i].GetAppKeyWord().ToString())
            {
                return loadedApps[i];
            }
        }

        for (int i = 0; i < appsPrefabs.Count; i++)
        {
            if (keyWord == appsPrefabs[i].GetAppKeyWord().ToString())
            {
                App app = Instantiate(appsPrefabs[i], parent);

                loadedApps.Add(app);

                return app;
            }
        }

        throw new System.Exception($"AppKeyWord '{keyWord}' does not exist in AppEnum. You should find '{keyWord}' in CSV file and change it");
    }
}
