using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

public class CSVLoader
{
    private bool _debug = true;
    private const string url = "https://docs.google.com/spreadsheets/d/*/export?format=csv";


    public async Task<string> DownloadTable(string tableID)
    {
        string actualUrl = url.Replace("*", tableID);
        
        using (UnityWebRequest request = UnityWebRequest.Get(actualUrl))
        {
            request.SendWebRequest();

            while (!request.isDone)
            {
                await Task.Yield();
            }


            if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError ||
                request.result == UnityWebRequest.Result.DataProcessingError)
            {
                Debug.LogError(request.error);
            }
            else
            {
                if (_debug)
                {
                    Debug.Log("Successful download");
                    Debug.Log(request.downloadHandler.text);
                }

                return request.downloadHandler.text;
            }
        }

        return "";
    }
}