﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GoogleFormSender: MonoBehaviour
{
    private string formURL;
    private event Action<bool> resultCallback;
    private List<ConfigForSendingToForm> configForSending = new List<ConfigForSendingToForm>();

    private readonly float timeForSending = 3f;

    public void SendData(string url, List<ConfigForSendingToForm> configs, Action<bool> callback)
    {
        formURL = url;
        configForSending = configs;
        resultCallback = callback;

        StartCoroutine(Send());   
    }

    private IEnumerator Send()
    {
        WWWForm form = new WWWForm();

        for (int i = 0; i < configForSending.Count; i++)
        {
            form.AddField(configForSending[i].FormEntryID, configForSending[i].InputText);
        }

        UnityWebRequest www = UnityWebRequest.Post(formURL, form);

        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.DataProcessingError || www.result == UnityWebRequest.Result.ProtocolError)
        {
            resultCallback?.Invoke(false);
        }
        else
        {
            resultCallback?.Invoke(true);
        }
    }
}
