using UnityEngine;

public class MainConfig : MonoBehaviour
{
    public string RawData { get; private set; } = "";

    private readonly string tableID = "1z1jGZfz3gz19PlSIPwS9V9m28lh4fVg6ZV9RjAJaNos";


    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private async void Start()
    {
        CSVLoader cSVLoader = new CSVLoader();

        RawData = await cSVLoader.DownloadTable(tableID);
    }
}
