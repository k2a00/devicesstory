﻿using UnityEngine;

public class SelectorView : MonoBehaviour
{
    [SerializeField] private RectTransform thisTransform;


    public void ReplaceSelector<T>(T parentObject) where T : ISelectable
    {
        thisTransform.parent = parentObject.GetTransform();
        
        thisTransform.anchorMin = new Vector2(0f, 0f);
        thisTransform.anchorMax = new Vector2(1f, 1f);

        thisTransform.offsetMin = new Vector2(-15, -15);
        thisTransform.offsetMax = new Vector2(15, 15);
    }
}

