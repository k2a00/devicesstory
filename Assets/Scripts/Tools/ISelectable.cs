using UnityEngine;

public interface ISelectable
{
    public Transform GetTransform();
}