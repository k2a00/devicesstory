using System.Collections.Generic;
using UnityEngine;

public class CasesLauncher: MonoBehaviour
{
    [SerializeField] private AppsStorage appsStorage;

    private KeyWords keyWords = new KeyWords();
    private Formatting formatting = new Formatting();
    private ResponsesChecker responsesChecker = new ResponsesChecker();
    
    private List<CaseController> caseControllers = new List<CaseController>();

    private string[] partLines;

    private readonly string caseBeginTag = "#Case";
    private readonly string caseEndTag = "#CaseEnd";
    private readonly string responseTag = "#Response";
    private readonly string responseEndTag = "#ResponseEnd";
    private readonly string additionToTag = "#AdditionTo";
    private readonly string additionEndTag = "#AdditionEnd";


    public void LaunchPartCases(string[] _partLines)
    {
        partLines = _partLines;

        int beginLine = 0;
        int endLine = 0;
        int caseID = 0;

        for (int i = 0; i < partLines.Length; i++)
        {
            if (beginLine == 0 && partLines[i].IndexOf(caseBeginTag) != -1)
            {
                beginLine = i + 1;
                
                string caseFullName = formatting.GetCellsValues(partLines[i])[0];
                caseID = int.Parse(caseFullName.Substring(caseBeginTag.Length));
            }

            if (partLines[i].IndexOf(caseEndTag) != -1)
            {
                endLine = i;
            }

            if (beginLine != 0 && endLine != 0)
            {
                string[] configLines = GetConfigLines(partLines, beginLine, endLine);

                int checkIndex = GetCheckIndex(formatting.GetSubstringAfterTag(keyWords.GetKey(Keys.Check), configLines));

                if (checkIndex == -1)
                {
                    beginLine = 0;
                    endLine = 0;

                    continue;
                }

                CaseController caseController = new CaseController(caseID);

                caseController.OnResponseGot.AddListener(Progress.Instance.AddResponse);
                caseController.OnResponseGot.AddListener(LaunchResponse);

                caseControllers.Add(caseController);
                
                ICaseConfigHandler caseConfigHandler = (ICaseConfigHandler)appsStorage.GetApp(formatting.GetSubstringAfterTag(keyWords.GetKey(Keys.App), configLines));
                
                caseConfigHandler.InitializeProcessing(FormatToCaseConfig(configLines, checkIndex, caseConfigHandler.GetDataConfig()), caseController);
                
                beginLine = 0;
                endLine = 0;

                LaunchAdditions(caseController, caseID.ToString());
            }
        }
    }

    private void LaunchResponse(ResponseConfig responseConfig)
    {
        string responseID = $"{responseConfig.CaseID}.{responseConfig.ResponseID + 1}";
        string responseBeginTag = $"{responseTag}{responseID}";

        string[] configLines = formatting.TryGetLinesBetweenTags(responseBeginTag, responseEndTag, partLines);

        if (configLines.Length == 0)
        {
            return;
        }

        int checkIndex = GetCheckIndex(formatting.GetSubstringAfterTag(keyWords.GetKey(Keys.Check), configLines));

        if (checkIndex == -1)
        {
            return;
        }

        ICaseConfigHandler caseConfigHandler = (ICaseConfigHandler)appsStorage.GetApp(formatting.GetSubstringAfterTag(keyWords.GetKey(Keys.App), configLines));

        CaseConfig caseConfig = FormatToCaseConfig(configLines, checkIndex, caseConfigHandler.GetDataConfig());

        if (!caseConfigHandler.TryToProcessCaseConfig(caseConfig))
        {
            caseConfigHandler.InitializeProcessing(caseConfig, responseConfig.CaseController);
        }

        LaunchAdditions(responseConfig.CaseController, responseID);
    }

    public void LaunchAdditions(CaseController caseController, string relatedID)
    {
        int beginLine = 0;
        int endLine = 0;
        string additionBeginTag = $"{additionToTag}{relatedID}";

        for (int i = 0; i < partLines.Length; i++)
        {
            if (beginLine == 0 && partLines[i].IndexOf(additionBeginTag) != -1)
            {
                beginLine = i + 1;
            }

            if (partLines[i].IndexOf(additionEndTag) != -1)
            {
                endLine = i;
            }

            if (beginLine != 0 && endLine != 0)
            {
                string[] configLines = GetConfigLines(partLines, beginLine, endLine);

                int checkIndex = GetCheckIndex(formatting.GetSubstringAfterTag(keyWords.GetKey(Keys.Check), configLines));

                if (checkIndex != -1)
                {
                    ICaseConfigHandler caseConfigHandler = (ICaseConfigHandler)appsStorage.GetApp(formatting.GetSubstringAfterTag(keyWords.GetKey(Keys.App), configLines));

                    CaseConfig caseConfig = FormatToCaseConfig(configLines, checkIndex, caseConfigHandler.GetDataConfig());

                    if (!caseConfigHandler.TryToProcessCaseConfig(caseConfig))
                    {
                        caseConfigHandler.InitializeProcessing(caseConfig, caseController);
                    }
                }

                beginLine = 0;
                endLine = 0;
            }
        }
    }

    private string[] GetConfigLines(string[] partLines, int beginLineIndex, int endLineIndex)
    {
        int linesCount = endLineIndex - beginLineIndex;
        string[] lines = new string[linesCount];

        for (int a = 0; a < linesCount; a++)
        {
            lines[a] = partLines[a + beginLineIndex];
        }

        return lines;
    }

    private int GetCheckIndex(string checkLine)
    {
        int checkIndex = 0;

        if (checkLine != "")
        {
            checkIndex = responsesChecker.GetCheckIndex(formatting.GetCellsValues(checkLine));
        }

        return checkIndex;
    }

    private CaseConfig FormatToCaseConfig(string[] configLines, int checkIndex, List<Keys> dataConfig)
    {
        Dictionary<Keys, string> data = new Dictionary<Keys, string>();

        for (int a = 0; a < dataConfig.Count; a++)
        {
            data.Add(dataConfig[a], formatting.GetSubstringAfterTag(keyWords.GetKey(dataConfig[a]), configLines));
        }

        if (data[Keys.Text] != "")
        {
            data[Keys.Text] = formatting.GetCellsValues(data[Keys.Text])[checkIndex];
        }

        if (data[Keys.Variants] != "")
        {
            data[Keys.Variants] = formatting.GetCellsValues(data[Keys.Variants])[checkIndex];
        }

        CaseConfig caseConfig = new CaseConfig(data);

        return caseConfig;
    }
}
