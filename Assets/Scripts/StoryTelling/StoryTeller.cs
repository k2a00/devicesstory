using System.Threading.Tasks;
using UnityEngine;

public class StoryTeller : MonoBehaviour
{
    [SerializeField] private MainConfig mainConfig;
    [SerializeField] private ProgressExchanger progressExchanger;
    [SerializeField] private CasesLauncher casesLauncher;

    private string periodRawData;


    private void Start()
    {
        progressExchanger.OnProgressLoaded.AddListener(OnProgressLoaded);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            OnProgressLoaded();
        }
    }

    private async void OnProgressLoaded()
    {
        periodRawData = await LoadPeriod();

        if (periodRawData == "")
        {
            FinishCharacterStory();
        }

        Formatting formatting = new Formatting();

        string beginTag = $"#Part{Progress.Instance.PartID}Begin";
        string endTag = $"#Part{Progress.Instance.PartID}End";

        string[] partLines = formatting.TryGetLinesBetweenTags(beginTag, endTag, formatting.SplitOnLines(periodRawData));

        if (partLines.Length == 0)
        {
            //TODO load next something

            return;
        }

        casesLauncher.LaunchPartCases(partLines);
    }

    private async Task<string> LoadPeriod()
    {
        PeriodLoader storyLoader = new PeriodLoader();

        return await storyLoader.TryLoadPeriodRawData(mainConfig.RawData);
    }

    private void FinishCharacterStory()
    {

    }
}
