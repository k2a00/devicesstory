using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResponseVariant : MonoBehaviour, ISelectable, IResponseSendable
{
    [SerializeField] private Text responseText;

    [HideInInspector] public UnityEvent<ResponseVariant> OnVariantSelected;

    private ResponseVariantInfo variantInfo;


    public void Init(ResponseVariantInfo _variantInfo)
    {
        variantInfo = _variantInfo;

        responseText.text = variantInfo.Text;
    }

    public void SelectVariant()
    {
        OnVariantSelected?.Invoke(this);
    }

    public Transform GetTransform()
    {
        return transform;
    }

    public ResponseVariantInfo GetResponseVariantInfo()
    {
        return variantInfo;
    }
}
