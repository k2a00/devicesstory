public class ResponsesChecker
{
    private readonly string differentlyVariantTag = "else"; //������ checkIndex, ����� �� ���� �� ������ ������� �� ���������

    public int GetCheckIndex(string[] cellsValuesOfCheck)
    {
        string responses = Progress.Instance.Responses;

        for (int i = 0; i < cellsValuesOfCheck.Length; i++)
        {
            string[] conditions = cellsValuesOfCheck[i].Split('|');

            int conditionsCompletedAmount = 0;

            for (int a = 0; a < conditions.Length; a++)
            {
                if (conditions[a].StartsWith(differentlyVariantTag))
                {
                    return i;
                }

                string responseID = conditions[a].Substring(0, conditions[a].IndexOf(':') + 1);

                int responseStartIndex = responses.IndexOf(responseID);

                if (responseStartIndex != -1)
                {
                    string conditionResponse = conditions[a].Substring(conditions[a].IndexOf(':') + 1);
                    
                    if (conditionResponse == responses.Substring(responseStartIndex + responseID.Length, 1))
                    {
                        conditionsCompletedAmount++;

                        if (conditionsCompletedAmount == conditions.Length)
                        {
                            return i;
                        }
                    }
                }
            }
        }

        int conditionsNonCompleted = -1;

        return conditionsNonCompleted;
    }
}
