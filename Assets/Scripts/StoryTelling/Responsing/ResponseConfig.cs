[System.Serializable]
public class ResponseConfig
{
    public int CaseID { get; private set; }
    public int ResponseID { get; private set; }
    public int Response { get; private set; }
    public CaseController CaseController { get; private set; }


    public ResponseConfig(int caseID, int responseID, int response, CaseController caseController)
    {
        CaseID = caseID;
        ResponseID = responseID;
        Response = response;

        CaseController = caseController;
    }
}
