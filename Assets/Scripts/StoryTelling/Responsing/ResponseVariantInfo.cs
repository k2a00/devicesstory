[System.Serializable]
public class ResponseVariantInfo
{
    public int Number { get; private set; }
    public int Index { get; private set; }
    public string Text { get; private set; }
 

    public ResponseVariantInfo(int number = -1, int index = -1, string text = "")
    {
        Number = number;
        Index = index;
        Text = text;
    }
}