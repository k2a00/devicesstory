using UnityEngine;
using UnityEngine.Events;

public class ResponseButton : MonoBehaviour, IResponseListener, IResponseSendable
{
    [HideInInspector] public UnityEvent<ResponseButton> OnResponseGot;

    private ResponseVariantInfo variantInfo = new ResponseVariantInfo();



    public void AddListener(IResponseListener responseListener)
    {
        OnResponseGot.AddListener(responseListener.SetResponse);
    }

    public void SendResponse()
    {
        if (variantInfo.Number == -1)
        {
            return;
        }

        OnResponseGot?.Invoke(this);

        variantInfo = new ResponseVariantInfo();
    }

    public void SetResponse<T>(T responseSender) where T : IResponseSendable
    {
        variantInfo = responseSender.GetResponseVariantInfo();
    }

    public ResponseVariantInfo GetResponseVariantInfo()
    {
        return variantInfo;
    }
}
