using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariantsOfResponse : MonoBehaviour
{ 
    [SerializeField] private ResponseVariant responseVariantPrefab;
    [SerializeField] private Transform parent;
    [SerializeField] private ResponseButton responseButton;
    [SerializeField] private SelectorView selector;
    
    private List<ResponseVariant> responseVariants = new List<ResponseVariant>();


    private void Start()
    {
        responseButton.OnResponseGot.AddListener(OnResponseSend);
    }

    public void InstantiateResponseVariants(CaseConfig caseConfig)
    {
        DestroyVariants();
        
        if (caseConfig.Data[Keys.Variants] == "")
        {
            return;
        }

        string[] variants = caseConfig.Data[Keys.Variants].Split('|');

        for (int i = 0; i < variants.Length; i++)
        {
            ResponseVariant variant = Instantiate(responseVariantPrefab, parent);

            variant.Init(new ResponseVariantInfo(int.Parse(variants[i].Substring(0, 1)), i, variants[i].Substring(1)));

            variant.OnVariantSelected.AddListener(responseButton.SetResponse);
            variant.OnVariantSelected.AddListener(selector.ReplaceSelector);

            responseVariants.Add(variant);
        }

        responseVariants[0].SelectVariant();
        selector.ReplaceSelector(responseVariants[0]);
        selector.gameObject.SetActive(true);
    }

    private void OnResponseSend(IResponseSendable responseSendable)
    {
        DestroyVariants();
    }

    private void DestroyVariants()
    {
        selector.gameObject.SetActive(false);
        selector.transform.SetParent(parent);

        if (responseVariants.Count != 0)
        {
            for (int i = 0; i < responseVariants.Count; i++)
            {
                Destroy(responseVariants[i].gameObject);
            }

            responseVariants.Clear();
        }
    }
}
