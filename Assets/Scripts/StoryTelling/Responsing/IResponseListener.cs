public interface IResponseListener
{
    public void SetResponse<T>(T responseSender) where T : IResponseSendable;
}
