public interface IResponseSendable
{
    public ResponseVariantInfo GetResponseVariantInfo();
}
