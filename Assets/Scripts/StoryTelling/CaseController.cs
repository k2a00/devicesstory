using UnityEngine.Events;

[System.Serializable]
public class CaseController : IResponseListener
{
    public UnityEvent<ResponseConfig> OnResponseGot;
    
    private int caseID;
    private int currentResponseID = 0;

    public CaseController(int _caseID)
    {
        caseID = _caseID;

        OnResponseGot = new UnityEvent<ResponseConfig>();
    }

    public void SetResponse<T>(T responseSender) where T : IResponseSendable
    {
        ResponseConfig responseConfig = new ResponseConfig(caseID, currentResponseID, responseSender.GetResponseVariantInfo().Number, this);

        currentResponseID++;

        OnResponseGot?.Invoke(responseConfig);
    }
}