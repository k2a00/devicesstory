using System.Threading.Tasks;

public class PeriodLoader
{
    private readonly string characterBeginTag = "#Character*Begin";
    private readonly string characterEndTag = "#Character*End";
    private readonly string periodTag = "#Period*";



    /// <summary>
    /// It can return empty string if next period does not exist
    /// </summary>
    public async Task<string> TryLoadPeriodRawData(string mainConfigRawData)
    {
        Formatting formatting = new Formatting();

        string characterID = Progress.Instance.CharacterID.ToString();
        string periodID = Progress.Instance.PeriodID.ToString();

        string beginTag = characterBeginTag.Replace("*", characterID);
        string endTag = characterEndTag.Replace("*", characterID);

        string[] lines = formatting.TryGetLinesBetweenTags(beginTag, endTag, formatting.SplitOnLines(mainConfigRawData));

        string periodURL = formatting.GetSubstringAfterTag(periodTag.Replace("*", periodID), lines);

        if (periodURL == "")
        {
            return "";
        }

        CSVLoader cSVLoader = new CSVLoader();

        return await cSVLoader.DownloadTable(periodURL);
    }
}
