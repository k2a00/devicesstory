using System.Collections.Generic;


[System.Serializable]
public class CaseConfig
{
    public Dictionary<Keys, string> Data { get; private set; } = new Dictionary<Keys, string>();

    
    public CaseConfig(Dictionary<Keys, string> data)
    {
        Data = data;
    }
}
